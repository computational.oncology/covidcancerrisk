# CovidCancerRisk

This repository contains the source code and final papers for the COVID Cancer risk project

CovidCancer1 was the first version of the PDF
CovidCancer2 is the second version of the PDF

There is associated code which require python 3.x with Numpy and Matplotlib installed to run

Please contact matthew.williams - at - imperial.ac.uk for further information