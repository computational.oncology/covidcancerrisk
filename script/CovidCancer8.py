"""
A script to estimate risks associated with age, sex, cancer and chemotherapy in Covid-19 and predict reduction in workload at different treatment benefit thresholds.

Parameters:

CFR_by_age: CFR per age band (9 age bands in all: 0-19; 20-29; 30-39; 40-49; 50-59; 60-69; 70-79; 80-89; 90+), expressed as fraction between 0 and 1

ORSexDeath: Increased risk of death with male sex; expressed as odds ratio

CFR_IFR = Ratio of CFR to IFR

ORCancerDeath: Increased risk of death with cancer with no active treatment; expressed as odds ratio
ORCancerChemoDeath: Increased risk of death with cancer and chemotherapy; expressed as odds ratio

Risk_of_infection: total number of infections by end of epidemic / total population; expressed as fraction between 0 and 1

Survival_with_chemo: OS with chemotherapy; expressed as fraction between 0 and 1
Survival_benefit_with_chemo = absolute difference in OS between chemotherapy vs no chemotherapy; expressed as fraction between 0 and 1

Age_group = age band of patient
Sex_group = sex of patient

"""

################## User parameters - Edit these ####################

CFR_by_age = {"0-19": 0.0000483251, "20-29": 0.0003331429, "30-39": 0.0005522170, "40-49": 0.0013102522, "50-59": 0.0051823089, "60-69": 0.0188038785, "70-79": 0.0599245650, "80-89": 0.1507791907, "90+": 0.2687281316, "total": 0.0192545954}

ORSexDeath = 1.77

CFR_IFR = 2.1

ORCancerDeath = 2.23
ORCancerChemoDeath = 4.54

Risk_of_infection = 1

Survival_with_chemo = 0.488
Survival_benefit_with_chemo = 0.054

Age_group = "60-69"
Sex_group = "M"

##################### Do not edit below here #########################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from pywaffle import Waffle

########## MODEL ##########
# calculate IFRs for death due to Covid-19 by age
IFR_by_age = {}

for age_group, CFR_age in CFR_by_age.items():
    IFR_age = CFR_age / CFR_IFR
    IFR_by_age[age_group] = IFR_age

# calculate IFRs for death due to Covid-19 by age and sex
CFR_male_by_age = {}
CFR_female_by_age = {}
all_CFR_by_age = {}

IFR_male_by_age = {}
IFR_female_by_age = {}
all_IFR_by_age = {}

for age_group, CFR in CFR_by_age.items():
    a = 1 - ORSexDeath
    b = 0.5 - CFR + ORSexDeath*0.5 + ORSexDeath*CFR
    c = -1 * (ORSexDeath * CFR * 0.5)
    d = (b**2) - (4*a*c)
    deaths_male_age = ((-b+math.sqrt(d))/(2*a))
    CFR_male_age = deaths_male_age / 0.5
    CFR_female_age = (CFR - deaths_male_age) / 0.5
    CFR_male_by_age[age_group] = CFR_male_age
    CFR_female_by_age[age_group] = CFR_female_age

for (age_group_M, CFR_male_age), (age_group_F, CFR_female_age) in zip(CFR_male_by_age.items(), CFR_female_by_age.items()):
    percent_CFR_age_M = CFR_male_age * 100
    percent_CFR_age_F = CFR_female_age * 100
    all_CFR_by_age[str(age_group_M)+"M"] = percent_CFR_age_M
    all_CFR_by_age[str(age_group_F)+"F"] = percent_CFR_age_F

for age_group, CFR_male_age in CFR_male_by_age.items():
    IFR_male_age = CFR_male_age / CFR_IFR
    IFR_male_by_age[age_group] = IFR_male_age

for age_group, CFR_female_age in CFR_female_by_age.items():
    IFR_female_age = CFR_female_age / CFR_IFR
    IFR_female_by_age[age_group] = IFR_female_age

for age_group, CFR_age in all_CFR_by_age.items():
    IFR_age = CFR_age / CFR_IFR
    all_IFR_by_age[age_group] = IFR_age

# calculate IFRs for death due to Covid-19 by age and sex with cancer and chemotherapy
IFR_cancer_M_by_age = {}
excess_IFR_cancer_M_by_age = {}
IFR_cancer_F_by_age = {}
excess_IFR_cancer_F_by_age = {}
all_IFR_cancer_by_age = {}
excess_all_IFR_cancer_by_age = {}

IFR_cancer_chemo_M_by_age = {}
excess_IFR_chemo_M_by_age = {}
IFR_cancer_chemo_F_by_age = {}
excess_IFR_chemo_F_by_age = {}
all_IFR_cancer_chemo_by_age = {}
excess_all_IFR_chemo_by_age = {}

for age_group, IFR_male_age in IFR_male_by_age.items():
    IFR_cancer_M = (1 * (ORCancerDeath * (IFR_male_age / (1 - IFR_male_age)))) / (1 + (ORCancerDeath * (IFR_male_age / (1 - IFR_male_age))))
    IFR_cancer_M_by_age[age_group] = IFR_cancer_M
    excess_IFR_cancer_M = IFR_cancer_M - IFR_male_age
    excess_IFR_cancer_M_by_age[age_group] = excess_IFR_cancer_M

for age_group, IFR_female_age in IFR_female_by_age.items():
    IFR_cancer_F = (1 * (ORCancerDeath * (IFR_female_age / (1 - IFR_female_age)))) / (1 + (ORCancerDeath * (IFR_female_age / (1 - IFR_female_age))))
    IFR_cancer_F_by_age[age_group] = IFR_cancer_F
    excess_IFR_cancer_F = IFR_cancer_F - IFR_female_age
    excess_IFR_cancer_F_by_age[age_group] = excess_IFR_cancer_F

for (age_group_M, IFR_male), (age_group_F, IFR_female) in zip(IFR_cancer_M_by_age.items(), IFR_cancer_F_by_age.items()):
    percent_IFR_cancer_M = IFR_male * 100
    percent_IFR_cancer_F = IFR_female * 100
    all_IFR_cancer_by_age[str(age_group_M)+"M"] = percent_IFR_cancer_M
    all_IFR_cancer_by_age[str(age_group_F)+"F"] = percent_IFR_cancer_F

for (age_group_M, excess_IFR_male), (age_group_F, excess_IFR_female) in zip(excess_IFR_cancer_M_by_age.items(), excess_IFR_cancer_F_by_age.items()):
    percent_excess_IFR_cancer_M = excess_IFR_male * 100
    percent_excess_IFR_cancer_F = excess_IFR_female * 100
    excess_all_IFR_cancer_by_age[str(age_group_M)+"M"] = percent_excess_IFR_cancer_M
    excess_all_IFR_cancer_by_age[str(age_group_F)+"F"] = percent_excess_IFR_cancer_F

for (age_group, IFR_male_age), (age_group_2, IFR_cancer_M) in zip(IFR_male_by_age.items(), IFR_cancer_M_by_age.items()):
    IFR_cancer_chemo_M = (1 * (ORCancerChemoDeath * (IFR_male_age / (1 - IFR_male_age)))) / (1 + (ORCancerChemoDeath * (IFR_male_age / (1 - IFR_male_age))))
    IFR_cancer_chemo_M_by_age[age_group] = IFR_cancer_chemo_M
    excess_IFR_chemo_M = IFR_cancer_chemo_M - IFR_cancer_M
    excess_IFR_chemo_M_by_age[age_group] = excess_IFR_chemo_M

for (age_group, IFR_female_age), (age_group_2, IFR_cancer_F) in zip(IFR_female_by_age.items(), IFR_cancer_F_by_age.items()):
    IFR_cancer_chemo_F = (1 * (ORCancerChemoDeath * (IFR_female_age / (1 - IFR_female_age)))) / (1 + (ORCancerChemoDeath * (IFR_female_age / (1 - IFR_female_age))))
    IFR_cancer_chemo_F_by_age[age_group] = IFR_cancer_chemo_F
    excess_IFR_chemo_F = IFR_cancer_chemo_F - IFR_cancer_F
    excess_IFR_chemo_F_by_age[age_group] = excess_IFR_chemo_F

for (age_group_M, IFR_male), (age_group_F, IFR_female) in zip(IFR_cancer_chemo_M_by_age.items(), IFR_cancer_chemo_F_by_age.items()):
    percent_IFR_cancer_chemo_M = IFR_male * 100
    percent_IFR_cancer_chemo_F = IFR_female * 100
    all_IFR_cancer_chemo_by_age[str(age_group_M)+"M"] = percent_IFR_cancer_chemo_M
    all_IFR_cancer_chemo_by_age[str(age_group_F)+"F"] = percent_IFR_cancer_chemo_F

for (age_group_M, excess_IFR_male), (age_group_F, excess_IFR_female) in zip(excess_IFR_chemo_M_by_age.items(), excess_IFR_chemo_F_by_age.items()):
    percent_excess_IFR_chemo_M = excess_IFR_male * 100
    percent_excess_IFR_chemo_F = excess_IFR_female * 100
    excess_all_IFR_chemo_by_age[str(age_group_M)+"M"] = percent_excess_IFR_chemo_M
    excess_all_IFR_chemo_by_age[str(age_group_F)+"F"] = percent_excess_IFR_chemo_F

# create tables of IFRs for death due to Covid-19
df_Covid = pd.concat([pd.DataFrame.from_dict(CFR_by_age, orient='index', columns=['CFR by age']), pd.DataFrame.from_dict(IFR_by_age, orient='index', columns=['IFR by age'])], axis=1)
df_Covid = df_Covid * 100
df_Covid_Cancer_Chemo = pd.concat([pd.DataFrame.from_dict(all_CFR_by_age, orient='index', columns=['CFR by age and sex']), pd.DataFrame.from_dict(all_IFR_by_age, orient='index', columns=['IFR by age and sex']), pd.DataFrame.from_dict(all_IFR_cancer_by_age, orient='index', columns=['IFR with cancer']), pd.DataFrame.from_dict(all_IFR_cancer_chemo_by_age, orient='index', columns=['IFR with chemotherapy'])], axis=1)
df_Covid.to_csv("Table 1.csv", index=True, header=True)
df_Covid_Cancer_Chemo.to_csv("Table 2.csv", index=True, header=True)

# plot stacked bar graph of IFRs for death due to Covid-19 by age and sex with and without cancer and chemotherapy
del all_IFR_by_age['totalM']
del all_IFR_by_age['totalF']
del excess_all_IFR_chemo_by_age['totalM']
del excess_all_IFR_chemo_by_age['totalF']
del excess_all_IFR_cancer_by_age['totalM']
del excess_all_IFR_cancer_by_age['totalF']

ind = np.arange(len(all_IFR_by_age))
p1 = plt.bar(ind, list(excess_all_IFR_chemo_by_age.values()), width=0.6, bottom=np.array(list(excess_all_IFR_cancer_by_age.values()))+np.array(list(all_IFR_by_age.values())))
p2 = plt.bar(ind, list(excess_all_IFR_cancer_by_age.values()), width=0.6, bottom=np.array(list(all_IFR_by_age.values())))
p3 = plt.bar(ind, list(all_IFR_by_age.values()), width=0.6)

plt.xticks(ind, list(all_IFR_by_age.keys()), rotation=90)
plt.yticks(np.arange(0, 100, 10))
plt.xlabel("Age and Sex")
plt.ylabel("IFR (%)")
plt.legend((p1[0], p2[0], p3[0]), ('excess IFR due to chemotherapy', 'excess IFR due to cancer', 'IFR due to age and sex'))
plt.tight_layout()
plt.savefig("Figure 1.png")
plt.clf()

# calculate increased risk of death from Covid-19 due to chemotherapy for individual patient with defined age and sex
risk_death_cancer_chemo_M_by_age = {}
risk_death_cancer_chemo_F_by_age = {}
excess_risk_death_chemo_M_by_age = {}
excess_risk_death_chemo_F_by_age = {}

for age_group, IFR_cancer_chemo_M in IFR_cancer_chemo_M_by_age.items():
    risk_death_cancer_chemo_M = IFR_cancer_chemo_M * Risk_of_infection
    risk_death_cancer_chemo_M_by_age[age_group] = risk_death_cancer_chemo_M

for age_group, IFR_cancer_chemo_F in IFR_cancer_chemo_F_by_age.items():
    risk_death_cancer_chemo_F = IFR_cancer_chemo_F * Risk_of_infection
    risk_death_cancer_chemo_F_by_age[age_group] = risk_death_cancer_chemo_F

for age_group, excess_IFR_chemo_M in excess_IFR_chemo_M_by_age.items():
    excess_risk_death_chemo_M = excess_IFR_chemo_M * Risk_of_infection
    excess_risk_death_chemo_M_by_age[age_group] = excess_risk_death_chemo_M

for age_group, excess_IFR_chemo_F in excess_IFR_chemo_F_by_age.items():
    excess_risk_death_chemo_F = excess_IFR_chemo_F * Risk_of_infection
    excess_risk_death_chemo_F_by_age[age_group] = excess_risk_death_chemo_F

increased_risk_death_chemo_M_by_age = {}
increased_risk_death_chemo_F_by_age = {}
all_increased_risk_death_chemo_by_age = {}

for (age_group, risk_death_cancer_chemo_M), (age_group_2, excess_risk_death_chemo_M) in zip(risk_death_cancer_chemo_M_by_age.items(), excess_risk_death_chemo_M_by_age.items()):
    increased_risk_death_chemo_M = ((excess_risk_death_chemo_M * Survival_with_chemo) / (1 - risk_death_cancer_chemo_M + excess_risk_death_chemo_M))
    increased_risk_death_chemo_M_by_age[age_group] = increased_risk_death_chemo_M

for (age_group, risk_death_cancer_chemo_F), (age_group_2, excess_risk_death_chemo_F) in zip(risk_death_cancer_chemo_F_by_age.items(), excess_risk_death_chemo_F_by_age.items()):
    increased_risk_death_chemo_F = ((excess_risk_death_chemo_F * Survival_with_chemo) / (1 - risk_death_cancer_chemo_F + excess_risk_death_chemo_F))
    increased_risk_death_chemo_F_by_age[age_group] = increased_risk_death_chemo_F

for (age_group_M, risk_male), (age_group_F, risk_female) in zip(increased_risk_death_chemo_M_by_age.items(), increased_risk_death_chemo_F_by_age.items()):
    percent_risk_chemo_M = risk_male * 100
    percent_risk_chemo_F = risk_female * 100
    all_increased_risk_death_chemo_by_age[str(age_group_M)+"M"] = percent_risk_chemo_M
    all_increased_risk_death_chemo_by_age[str(age_group_F)+"F"] = percent_risk_chemo_F

Age_sex_group = Age_group + Sex_group
print(all_increased_risk_death_chemo_by_age[Age_sex_group])

# create pictogram of expected deaths and survivals with and without chemotherapy for population of patients with defined age and sex
Deaths_Covid_with_chemo = all_IFR_cancer_chemo_by_age[Age_sex_group]
Deaths_cancer_with_chemo = (100 - Deaths_Covid_with_chemo) * (1 - Survival_with_chemo)
Survivals_cancer_with_chemo = (100 - Deaths_Covid_with_chemo) * Survival_with_chemo

Deaths_Covid_without_chemo = all_IFR_cancer_by_age[Age_sex_group]
Deaths_cancer_without_chemo = (100 - Deaths_Covid_without_chemo) * (1 - (Survival_with_chemo - Survival_benefit_with_chemo))
Survivals_cancer_without_chemo = (100 - Deaths_Covid_without_chemo) * (Survival_with_chemo - Survival_benefit_with_chemo)

plt.figure(FigureClass=Waffle, rows=10, columns=10, values={'Covid-19 deaths': Deaths_Covid_with_chemo, 'Cancer deaths': Deaths_cancer_with_chemo, 'Alive at 5 years': Survivals_cancer_with_chemo}, legend={'loc': 'upper left', 'bbox_to_anchor': (1, 1)}, cmap_name="tab10", icons='male', font_size=20, starting_location='NW', vertical=True)
plt.savefig("Figure 2a.png")
plt.clf()

plt.figure(FigureClass=Waffle, rows=10, columns=10, values={'Covid-19 deaths': Deaths_Covid_without_chemo, 'Cancer deaths': Deaths_cancer_without_chemo, 'Alive at 5 years': Survivals_cancer_without_chemo}, legend={'loc': 'upper left', 'bbox_to_anchor': (1, 1)}, cmap_name="tab10", icons='male', font_size=20, starting_location='NW', vertical=True)
plt.savefig("Figure 2b.png")
plt.clf()